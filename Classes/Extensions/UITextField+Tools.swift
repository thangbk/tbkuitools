//
//  UITextField+Tools.swift
//  TBKUITools
//
//  Created by Thang Nguyen Manh on 10/22/19.
//

import UIKit

extension UITextField {

    public convenience init(placeholder: String) {
        self.init()
        self.placeholder = placeholder
    }

}
