//
//  CircularImageView.swift
//  TBKUITools
//
//  Created by Thang Nguyen Manh on 10/22/19.
//

import UIKit

@available(iOS 9.0, *)
open class CircularImageView: UIImageView {
    
    public init(width: CGFloat, image: UIImage? = nil) {
        super.init(image: image)
        contentMode = .scaleAspectFill
        if width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        heightAnchor.constraint(equalTo: widthAnchor).isActive = true
        clipsToBounds = true
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.width / 2
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}
