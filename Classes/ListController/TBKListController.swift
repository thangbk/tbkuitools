//
//  TBKListController.swift
//  TBKUITools
//
//  Created by Thang Nguyen Manh on 10/22/19.
//

import UIKit

/**
 Convenient list component where a Header class is not required.
 
 ## Generics ##
 T: the cell type that this list will register and dequeue.
 
 U: the item type that each cell will visually represent.
 */

@available(iOS 9.0, *)
open class TBKListController<T: TBKListCell<U>, U>: TBKListHeaderController<T, U, UICollectionReusableView> {
}
