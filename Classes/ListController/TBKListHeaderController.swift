//
//  TBKListHeaderController.swift
//  TBKUITools
//
//  Created by Thang Nguyen Manh on 10/22/19.
//

import UIKit

/**
 ListHeaderController helps register, dequeues, and sets up cells with their respective items to render in a standard single section list.

 ## Generics ##
 T: the cell type that this list will register and dequeue.
 
 U: the item type that each cell will visually represent.
 
 H: the header type above the section of cells.
 
 */
@available(iOS 9.0, *)
open class TBKListHeaderController<T: TBKListCell<U>, U, H: UICollectionReusableView>: TBKListHeaderFooterController<T, U, H, UICollectionReusableView> {
}
