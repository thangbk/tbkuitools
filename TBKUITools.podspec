Pod::Spec.new do |s|
s.name              = 'TBKUITools'
s.version           = '1.0.13'
s.summary           = 'TBKUITools'
s.homepage          = 'https://gitlab.com/thangbk/TBKUITools'
s.ios.deployment_target = '9.0'
s.platform = :ios, '9.0'
s.license           = {
:type => 'MIT',
:file => 'LICENSE'
}
s.author            = {
'YOURNAME' => 'ThangBK'
}
s.source            = {
:git => 'https://gitlab.com/thangbk/TBKUITools.git',
:tag => "#{s.version}" }
s.swift_version = '4.0'
s.framework = "UIKit"
s.source_files      = 'TBKUITools*' , 'Classes/*','Classes/Extensions/*','Classes/Form/*','Classes/ListController/*','Classes/TBKUITools/*','Classes/UIKitSubclasses/*', 'Classes/Transition/*', 'Resource/*'
s.requires_arc      = true
end
